// let flag = true
// let flag = 65
// let flag = '65'
// let flag = 'Sixty Five'
let flag =6
// let flag2 = 8
let flag2 = '6'

// flag = parseInt(flag)
// flag = flag/'13'  // '65'
// flag = Number(flag)
// console.log(typeof flag)
// flag ++ //Post increment
// console.log(flag++)

// ++flag //Pre Increment
// console.log(++flag)

console.log(flag == flag2)
console.log(flag === flag2) //strict comparison : value as well as dataype

//typecasting to a String
// flag = String(flag)

// flag = flag.toString()
// flag = flag + ' number' // concatenating to a string will always return a string
// console.log(typeof flag)

let age = 70;
let isRetired = age > 60 ? true : false
console.log('Retired: '+isRetired)
console.log()
switch(age){
    case 60:
        console.log('age is : '+age)
        break;
    case 70:
        console.log('age is : '+age);
        break;
    case 80:
        console.log('age is : '+age);
        break;
    default:
        console.log('No match');

}

let i=1;
console.log("===========Table of 5=======")
while(i<=10){ //when number of iteration is not known
    console.log("Loop iteration "+i)
    console.log('5 * '+i + ' = '+ 5*i)
    i++;
}
console.log("===========Table of 6=======")
for(let j = 1;j<=10;j++){
    j+=2;
    console.log("Loop iteration "+i)
    console.log('5 * '+i + ' = '+ 5*i)
}

//do while
console.log("==========")
let m=1;
do{
    console.log(m*3)
    m++
}while(m<10)

let iterations =0;
top:
for(let i=0;i<5;i++){
    for(let j=0;j<5;i++){
        iterations++;
        if(i===2 && j===2){
            break top;
        }
    }
}
console.log(iterations)
