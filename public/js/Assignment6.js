
let i = 1;
var employeesArr = []
var empName
var empID
var empDesignation
var empHours

    class Employee {
        #nameValue
        #idValue
        #designationValue
        #hoursValue
        #salaryValue

        constructor (name, id, designation, hours, salary) {
            this.name = name
            this.id = id
            this.designation = designation
            this.hours = hours
            this.salary = salary
        }
        
        get name() {
            return this.#nameValue
        }
        set name(name) {
            if (name === "") {
                alert ("Employee Name cannot be empty")
            } else {
                this.#nameValue = name;
            } 
        }

        get id() {
            return this.#idValue
        }
        set id(id) {
            if (id === "") {
                alert ("Employee ID cannot be empty")
            } else {
                this.#idValue = id;
            } 
        }

        get designation() {
            return this.#designationValue
        }
        set designation(designation) {
            if (designation === "") {
                alert ("Designation cannot be empty")
            } else {
                this.#designationValue = designation;
            } 
        }

        get hours() {
            return this.#hoursValue
        }
        set hours(hours) {
            if (hours === "") {
                alert ("Hours Worked cannot be empty")
            } else {
                this.#hoursValue = hours;
            } 
        }

        get salary() {
            return this.#salaryValue
        }
        set salary(salary) {
            if (salary === "") {
                alert ("Salary cannot be calculated")
            } else {
                this.#salaryValue = salary;
            }
        }
    }


function addEmployee(){
   
   empName = document.getElementById("empName").value
   empID = document.getElementById("empId").value
   empDesignation = document.getElementById("designation").value
   empHours = document.getElementById("hoursWorked").value 

   if(empDesignation.toLowerCase() == "manager"){
        empSalary = empHours * 50
    } else 
    if (empDesignation.toLowerCase() == "consultant" ){
        empSalary = empHours * 30
    } else
    if(empDesignation.toLowerCase() == "trainee"){
        empSalary = empHours * 20    
    } else
    {   empSalary = 0
    }

    console.log(empName, empID, empDesignation, empHours, empSalary)
    
   const emp = new Employee(empName, empID, empDesignation, empHours, empSalary)

    console.log(emp)
    employeesArr.push(emp)

    text = "Employee" +i+" added successfully"

    document.getElementById("display").innerHTML = text

    document.getElementById("empName").value = ""
    document.getElementById("empId").value = ""
    document.getElementById("designation").value = ""
    document.getElementById("hoursWorked").value = ""
    i++   
}

function maxSalEmployee(){

var maxSalary = Math.max(...employeesArr.map(e => e.salary));

var obj = employeesArr.find(employee => employee.salary === maxSalary);

text = obj.name + " who is a "+ obj.designation + " gets the highest salary of $"+ obj.salary
document.getElementById("display").innerHTML = text

}  

