let myFunc= function(){
    console.log('Example of a function expression')
}

setTimeout(() => consolelog(('Display after 5 seconds'),500))

(function(){
    console.log('Anonymous function')
})();

let sum = function(a,b){
    return a+b;
}

let arrow = ()=> console.log('Arrow function')