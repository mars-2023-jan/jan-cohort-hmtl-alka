
let i = 1;

function addEmployee() {
    let employee = {}
    // alert("Adding employee "+i)
    employee.empId = document.getElementById("empId").value
    employee.empName = document.getElementById("empName").value
    employee.designation = document.getElementById("designation").value
    employee.hoursWorked = document.getElementById("hoursWorked").value

    if (employee.designation.toLowerCase() == 'manager') {
        employee.salary = employee.hoursWorked * 50
    }
    if (employee.designation.toLowerCase() == 'consultant') {
        employee.salary = employee.hoursWorked * 30
    }
    if (employee.designation.toLowerCase() == 'trainee') {
        employee.salary = employee.hoursWorked * 20
    }

    let empArr = localStorage.getItem("employeesArr") || [];
    if (empArr.length != 0) {
        empArr = JSON.parse(empArr);
    }


    empArr.push(employee)
    localStorage.setItem("employeesArr", JSON.stringify(empArr));

    text = 'Employee ' + i + ' Saved Successfully'

    document.getElementById("empId").value = ''
    document.getElementById("empName").value = ''
    document.getElementById("designation").value = ''
    document.getElementById("hoursWorked").value = ''

    document.getElementById("display").innerHTML = text
    i++
}

function maxSalEmployee() {
    let employeesArr = JSON.parse(localStorage.getItem("employeesArr"));

    let highestSalaryEmp = employeesArr[0];

    for (let j = 0; j < (employeesArr.length); j++) {
        if (highestSalaryEmp.salary < employeesArr[j].salary) {
            highestSalaryEmp = employeesArr[j]
            // alert("inside if")
        }

    }
    text = highestSalaryEmp.empName + ' who is a ' + highestSalaryEmp.designation + ' gets the highest salary of $' + highestSalaryEmp.salary

    document.getElementById("display").innerHTML = text
}