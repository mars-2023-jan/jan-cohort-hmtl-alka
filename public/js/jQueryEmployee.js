
let i = 1;
var employeesArr = []
var empName
var empID
var empDesignation
var empHours
var msg = "";

class Employee {
    #nameValue
    #idValue
    #designationValue
    #hoursValue
    #salaryValue

    constructor(name, id, designation, hours, salary) {
        this.name = name
        this.id = id
        this.designation = designation
        this.hours = hours
        this.salary = salary
    }

    get name() {
        return this.#nameValue
    }
    set name(name) {
        if (name === "") {
            alert("Employee Name cannot be empty")
        } else {
            this.#nameValue = name;
        }
    }

    get id() {
        return this.#idValue
    }
    set id(id) {
        if (id === "") {
            alert("Employee ID cannot be empty")
        } else {
            this.#idValue = id;
        }
    }

    get designation() {
        return this.#designationValue
    }
    set designation(designation) {
        if (designation === "") {
            alert("Designation cannot be empty")
        } else {
            this.#designationValue = designation;
        }
    }

    get hours() {
        return this.#hoursValue
    }
    set hours(hours) {
        if (hours === "") {
            alert("Hours Worked cannot be empty")
        } else {
            this.#hoursValue = hours;
        }
    }

    get salary() {
        return this.#salaryValue
    }
    set salary(salary) {
        if (salary === "") {
            alert("Salary cannot be calculated")
        } else {
            this.#salaryValue = salary;
        }
    }
}

$("#add").on("click", function () {
    alert("Adding employee")
    empName =$("#empName").val()
   empID = $("#empId").val()
   empDesignation = $("#designation").val()
   empHours = $("#hoursWorked").val()

    if (empDesignation.toLowerCase() == "manager") {
        empSalary = empHours * 50
    } else
        if (empDesignation.toLowerCase() == "consultant") {
            empSalary = empHours * 30
        } else
            if (empDesignation.toLowerCase() == "trainee") {
                empSalary = empHours * 20
            } else {
                empSalary = 0
            }

    console.log(empName, empID, empDesignation, empHours, empSalary)

    const emp = new Employee(empName, empID, empDesignation, empHours, empSalary)

    console.log(emp)
    employeesArr.push(emp)

    msg = "Employee" + i + " added successfully"

    $("#display").html(msg)

    $("#empName").val("")
    $("#empId").val("")
    $("#designation").val("")
    $("#hoursWorked").val("")
    i++
})

    $( "#employeeForm" ).on("submit", function( event ) {
        event.preventDefault();
    const maxSalEmployee = employeesArr.reduce((previous, current) => { return previous.salary > current.salary ? previous : current })
    msg = maxSalEmployee.name + " who is a " + maxSalEmployee.designation + " gets the highest salary of $" + maxSalEmployee.salary
    $("#display").html(msg)

})

