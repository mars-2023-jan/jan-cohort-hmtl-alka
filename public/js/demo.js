// let a = 12;
// let b = 14;
// console.log("Sum of two numbers "+ (a+b));
let studName  //variable declaration
studName="Martin" //variable definition
let studAge= 28
console.log(`Hello ${studName}`) //Interpolation
console.log(studAge/0) //returning infinity - no error
console.log('studAge type:'+typeof (studAge))

const SCHOOL_NAME = 'ABC'; // should be in caps
console.log(studName)
// if(studAge > 20){
if(studAge){
    // let message = "Student is over age"
    var message = "Student is over age"
}
console.log(message)

let grade = 'A'
// let grade = 'A+' //cannot declare again the same variable
grade = 'A+'
console.log(grade)

// SCHOOL_NAME = 'xyz'; // constant value cannot be re-assigned
console.log(SCHOOL_NAME)

//Any non empty string is equivalent to true
//Any non zero number is equivalent to true