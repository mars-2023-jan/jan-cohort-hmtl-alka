let employee = {    //object literal representing 1 employee
    'firstname' : 'John',
    'lastname' : 'Doe',
    'age' : 32,
    'designation' :'Manager',
    'home address' : 'Milwaukee',
     getFullName(){
        return this.firstname+' '+this.lastname
     }
}

function Car(make, model, year, color){
    this.make = make
    this.model = model
    this.year = year
    this.color = color
}

let car1 = new Car('Honda','Accord','2016','Blue')
let car2 = new Car('Ford','Freestyle','2016','Red')
let car3 = new Car('Toyata','Forrunner','2016','White')
console.log("=============================")

console.log(car1.model)
console.log(car2.make)

console.log("=============================")

let nameArr = ['Keith','Clara']

employee.firstname = 'Pete' //updating a property

employee.contact = 24725892 // adding a property to the object

delete employee['home address'] // deleting a property

console.log(typeof employee)

console.log(typeof nameArr)

console.log(employee.lastname) //won't work with home address having space
console.log(employee['lastname'])
console.log(employee["home address"])

console.log(employee)

console.log('age' in employee) // true as it is present
console.log('ssn' in employee) //false as ssn is not present

for(const key in employee){
    console.log(key) //accessing properties
    console.log(employee[key]) //accessing property values
}
console.log("===================")

console.log(employee.getFullName())