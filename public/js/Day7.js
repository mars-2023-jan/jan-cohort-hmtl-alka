// function getArr(){
//     // return [56,78,32,45]
//     return [56,78,32,45,98,21,11]

// }

// let numbers = getArr();

// // let [a, b, c, d] = getArr();
// let [a,b, ...c] = getArr()

// let [d,e, ...f] = c

// // console.log(numbers[0])
// console.log(c)
// console.log(e)
// console.log(f)

// function getArr1(){
//     return null
// }

// let [a1=10,b1=20] =getArr1() || []

// console.log(e)

// function getNestedArray(){
//     return[
//         'David',
//         'Peter',
//         [
//             'Red',
//             'Blue',
//             'Green'
//         ]
//     ]
// }

// let [
//     name1,
//     name2,
//     [
//         color1,
//         color2,
//         color3
//     ]
// ] = getNestedArray()

// console.log(name1)

// function calculate(){
// return 
// a3+b3
// }
// //swapping 2 variables
// let a3=10 ;
// let b3 = 20;

// [a3,b3] = [b3,a3]

// console.log('a : '+a3)
// console.log('b: '+b3)

// let[sum,average,multiplication] = calculate(23,25)


let person = {
    firstName : 'Michael',
    lastName: 'Jordon',
    age: 31
}

// let {firstName : fName, middleName : mName='Clarke',lastName : lName,age :age} = person

// console.log(fName+ ' '+lName+':' +age)
// console.log(mName)

// function person(){
//     return null
// }

// let {firstName : fName, middleName : mName='Clarke',lastName : lName,age :age} = person || {}

// console.log(fName+ ' '+lName+':' +age)

let display= ({firstName,lastName})=>console.log(firstName + ' ' +lastName)

display(person)

let salaries = {
    "John" : 500,
    "Mary" : 800,
    "Carl" : 600
}

// let highSalary= ({firstName,lastName})=>console.log(firstName + ' ' +lastName)
 
// let entries = Object.entries(salaries)

// console.log(entries)


function maxSalary(salaries){
    let maxSalary = 0
    let maxName = null
    for(const[name,salary] of Object.entries(salaries)){
        if(maxSalary < salary){
            maxSalary = salary
            maxName = name
        }
    }
    return maxName
}
console.log(maxSalary(salaries))

console.log(Object.keys(salaries))

console.log(Object.values(salaries))

//use reduce method of arrays to find the person with max salary

//Example of spread operator

let a = [1,3,5,7]
let b = [2,4,6,8]

let car = {
    make : 'Ford',
    model: 'Mustang',
    year: 1999
}

let updatedCar = {
    color : 'red',
    year : 2018,
}

let c = {...car, ...updatedCar}

// let c = [...a, ...b]

console.log(c)
